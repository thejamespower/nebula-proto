var express = require('express'),
  app = express(),
  fs = require('fs'),
  bodyParser = require('body-parser'),

  port = 3000;

// Serve static files
app.use(express.static('public'));
app.use(bodyParser.text({
  'type': 'text/html'
}));

app.get('/', function(req, res) {
  res.sendFile('public/index.html', {
    root: './'
  });
});

app.get('/viewer', function(req, res) {
  res.redirect(301, '/#/viewer');
});

app.get('/getJson', function(req, res) {
  var jsonBuff = fs.readFileSync('./service/content.json'),
    json = JSON.parse(jsonBuff);
  // console.log(json);
  res.status(200).send(json);
});

app.get('/getHtml', function(req, res) {
  var html = fs.readFileSync('./service/content.html');
  res.status(200).send(html);
});

app.post('/sendHtml', function(req, res) {

  var posted = req.body;
  // console.log('Post recieved!' + posted);
  fs.writeFile('./service/content.html', posted, function(err) {
    if (err) {
      return console.log(err);
    } else {
      res.status(200)
      res.setHeader('Content-Type', 'text/plain')
      res.write('You posted:\n')
      res.end(posted)
    }
  });

});

app.listen(port, function() {
  console.log('Nebula server listening on port:' + port);
});
