var nebulaEditor = angular.module('nebulaEditor', ['textAngular', 'ui.router', 'xeditable']);

nebulaEditor.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/");
  // Now set up the states
  $stateProvider
    .state('home', {
      url: "/",
      templateUrl: "js/views/editor.html",
      controller: "Editor",
    })
    .state('viewer', {
      url: "/viewer",
      templateUrl: "js/views/viewer.html",
      controller: "Viewer",
    });
});

nebulaEditor.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});
