nebulaEditor.controller('Viewer', ['$scope', '$http', '$interval', function($scope, $http, $interval) {

  $scope.getContent = function() {

    console.log('Getting content');

    // Get the package json
    $http.get('/getJson')
      .then(function(response) {

        $scope.config = response.data;

      });

    // Get HTML content
    $http.get('/getHtml')
      .then(function(response) {

        $scope.loadedHtml = response.data;

        $scope.htmlViewer = $scope.loadedHtml;
      });
  };
  $scope.getContent();

  $interval($scope.getContent, 1000);

}]);
