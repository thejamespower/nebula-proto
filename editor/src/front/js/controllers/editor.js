nebulaEditor.controller('Editor', ['$scope', '$http', function($scope, $http) {

  $scope.edited = 0;

  $scope.introduction = 'This is a basic test editor.';

  // Get the package json
  $http.get('/getJson')
    .then(function(response) {

      $scope.config = response.data;

    });

  // Get HTML content
  $http.get('/getHtml')
    .then(function(response) {

      $scope.loadedHtml = response.data;

      $scope.htmlContent = $scope.loadedHtml;
    });

  $scope.edit = function() {
    $scope.edited++;
    // console.log('Post sent : ' + $scope.htmlContent);
    $http.post('/sendHtml', $scope.htmlContent, {
      'headers': {
        'Content-Type': 'text/html'
      }
    });
  };

}]);
