'use strict';

var gulp = require('gulp'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync'),
  autoprefixer = require('autoprefixer'),
  postcss = require('gulp-postcss'),
  sourcemaps = require('gulp-sourcemaps'),
  nodemon = require('gulp-nodemon'),

  // Delay before reloading browsers, async issues
  BROWSER_SYNC_RELOAD_DELAY = 800,

  // Paths
  srcPath = './src/',
  publicPath = './public/',
  servicePath = './service/';

// Handle bower files, currently just moves them to public folder
gulp.task('bower', function() {
  gulp.src('bower_components/**/*.*')
    .pipe(gulp.dest(publicPath + '/bower_components/'));
});

// Start nodemon service
gulp.task('nodemon', function(cb) {
  var called = false;
  return nodemon({

      // nodemon our expressjs server
      script: 'service/index.js',

      // watch core server file(s) that require server restart on change
      watch: ['service/index.js']
    })
    .on('start', function onStart() {
      // ensure start only got called once
      if (!called) {
        cb();
      }
      called = true;
    })
    .on('restart', function onRestart() {
      // reload connected browsers after a slight delay
      setTimeout(function reload() {
        browserSync.reload({
          stream: false
        });
      }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

gulp.task('sass', function() {
  var sassOptions = {
      errLogToConsole: true,
      outputStyle: 'expanded'
    },
    autoprefixerOptions = {
      browsers: ['last 2 versions']
    };
  gulp.src(srcPath + 'front/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(sourcemaps.write())
    // autoprefixer currently broken
    // .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(publicPath + '/css/'))
    .pipe(browserSync.stream());
});

// front js task
// currently just copies files
// to do: lint/format and error check
gulp.task('front-js', function() {
  gulp.src(srcPath + 'front/js/**/*.*')
    .pipe(gulp.dest(publicPath + '/js/'));

  setTimeout(function reload() {
    browserSync.reload({
      stream: false
    });
  }, BROWSER_SYNC_RELOAD_DELAY);
});

// back js task
// currently just copies files
// to do: lint/format and error check
gulp.task('back-js', function() {
  gulp.src(srcPath + 'back/**/*.*')
    .pipe(gulp.dest(servicePath));

  setTimeout(function reload() {
    browserSync.reload({
      stream: false
    });
  }, BROWSER_SYNC_RELOAD_DELAY);
});

// copy index.html
// currently just copies files
// to do: lint/format and error check
gulp.task('index', function() {
  gulp.src(srcPath + 'front/index.html')
    .pipe(gulp.dest(publicPath));

  setTimeout(function reload() {
    browserSync.reload({
      stream: false
    });
  }, BROWSER_SYNC_RELOAD_DELAY);
});

gulp.task('browser-sync', ['nodemon'], function() {

  // for more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync({

    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:3000',

    // informs browser-sync to use the following port for the proxied app
    // notice that the default port is 3000, which would clash with our expressjs
    port: 4000
  });

  gulp.watch("bower_components/**/*.*", ['bower']);
  gulp.watch("src/front/scss/**/*.*", ['sass']);
  gulp.watch("src/front/index.html", ['index']);
  gulp.watch("src/front/js/**/*.*", ['front-js']);
  gulp.watch("src/back/**/*.*", ['back-js']);
  // gulp.watch("public/**/*.*").on('change', browserSync.reload);
});

gulp.task('default', ['index', 'front-js', 'back-js', 'sass', 'bower', 'browser-sync']);
