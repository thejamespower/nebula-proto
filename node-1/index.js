// We need the http API module
var http = require('http');

// The username to connect to the Treehouse API
var username = 'jamespower';

// Check everything is okay, mandatory hello world statement etc
console.log('Hello, world!');
console.log('Connecting to the Treehouse account of ' + username + '...');

// Use the http module to connect to the Treehouse API URL for the given username
var request = http.get('http://teamtreehouse.com/' + username + '.json', function(response) {

  // Somehwere to store the json from the response
  var responseData = '';

  // When the response gets a data event,
  // i.e., when we recieve some data from the request
  response.on('data', function(chunk) {

    // Add the recieved data to the json variable we created
    responseData += chunk;
  });

  // It's a good idea to wait for the http request to end before logging out the result
  response.on('end', function() {

    if (response.statusCode === 200) {

      // Response data is a string, we need to process it properly to make it JSON-ey goodness
      // We'll store this JSON object in a 'profile' variable
      var profile = JSON.parse(responseData);

      // Print out some of the stats from Treehouse :D
      console.log(username + ' has ' + profile.badges.length + ' badge(s), amazing!');
    }
  });
});

// Basic error handling for our request
request.on('error', function(error) {
  console.error('Unforunately there was an error.\n Here is the error message: ' + error.message);
});
