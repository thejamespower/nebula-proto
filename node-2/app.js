//Problem: We need a simple way to look at a user's badge count and JavaScript point from a web browser
//Solution: Use Node.js to perform the profile look ups and server our template via HTTP

var http = require('http'),
  router = require('./router.js'),
  port = 3000,
  // ip = '192.168.0.4';
  ip = '10.0.1.16';

// Create a web server
http.createServer(function (request, response) {
  router.home(request, response);
  router.user(request, response);
}).listen(port, ip);

console.log('Server started');
