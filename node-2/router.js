var Profile = require('./profile.js'),
  renderer = require('./renderer.js'),
  querystring = require('querystring'),
  header = {
    'Content-Type': 'text/html'
  };

// Handle HTTP route GET / and POST / i.e. Home
var home = function(request, response) {
  //if url == "/" && GET
  if (request.url === '/') {
    if (request.method.toLowerCase() === 'get') {
      //show search
      response.writeHead(200, header);
      renderer.view('header', {}, response);
      renderer.view('search', {}, response);
      renderer.view('footer', {}, response);
      response.end();
    } else {
      //if url == "/" && POST

      // get the post data from body
      request.on('data', function(body) {
        // extract the username
        var query = querystring.parse(body.toString());

        //redirect to /:username
        response.writeHead(303, {
          'Location': '/' + query.username
        });
        response.end();
      });

    }
  }
};

// Handle HTTP route GET /:username i.e. /chalkers
var user = function(request, response) {
  //if url == "/...."
  var username = request.url.replace('/', '');
  if (username.length > 0) {
    response.writeHead(200, header);
    renderer.view('header', {}, response);

    //get json from Treehouse
    var profile = new Profile(username);
    //on "end"
    profile.on('end', function(json) {

      //store the values we need
      var values = {
        avatarUrl: json.gravatar_url,
        username: json.profile_name,
        badges: json.badges.length,
        javascriptPoints: json.points.JavaScript
      };

      //show profile
      renderer.view('profile', values, response);
      // response.write(values.username + ' has ' + values.badges + ' badges\n');
      renderer.view('footer', {}, response);
      response.end();
    });

    //on "error"
    profile.on('error', function(error) {
      //show error
      renderer.view('header', {}, response);

      renderer.view('error', {
        errorMessage: error.message
      }, response);
      // response.write('Error:' + error.message + '\n');
      renderer.view('search', {}, response);
      renderer.view('footer', {}, response);

      response.end();
    });
  }
};

module.exports.home = home;
module.exports.user = user;
