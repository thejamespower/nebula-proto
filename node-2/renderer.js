var fs = require('fs');
var merge = function(values, content) {
  // Loop through keys
  for (var key in values) {
    // Replace all {{key}} with the value for value object
    // values.key = values['key']
    content = content.replace('{{' + key + '}}', values[key]);
  }
  // return merged content
  return content;
};

var view = function(name, values, response) {
  // Read from the template (view) file
  var file = fs.readFileSync('./views/' + name + '.html', {
    encoding: 'utf-8'
  });
  // Insert values in to the content
  file = merge(values, file);
  // Write out contents to the response
  response.write(file);
};

module.exports.view = view;
